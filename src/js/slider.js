$(document).ready(function () {
    $('.example-group').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        draggable: false,
        responsive: [
            {
                breakpoint: 651,
                settings: {
                    dots: false
                }
            }
        ]
    });
});